/* eslint-disable react-hooks/exhaustive-deps */
import './App.css';
import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Albums from './Components/Albums';
import Album from './Components/Album';
import Photos from './Components/Photos';
import Navbar from "./Components/Navbar";
import Photo from './Components/Photo';
import Login from './Components/Login';
import CompletePhoto from './Components/CompletePhoto';
import CrudAlbum from './Components/CrudAlbum';
import CrudPhoto from './Components/CrudPhoto';

function App() {
  const isLoggedIn = sessionStorage.getItem('UserId');
  if(isLoggedIn){
    return (
      <BrowserRouter>
        <Navbar />
        <Switch>
          <Route path="/" exact>
            <Albums />
          </Route>
          <Route path="/photos">
            <Photos />
          </Route>
          <Route path="/complete">
            <CompletePhoto />
          </Route>
          <Route path="/photo">
            <Photo />
          </Route>  
          <Route path="/albums">
            <Album />
          </Route>
          <Route path="/crudalbum">
            <CrudAlbum />
          </Route>
          <Route path="/crudphoto">
            <CrudPhoto />
          </Route>       
        </Switch>
      </BrowserRouter>
    );
  }else{
    return(
      <BrowserRouter>
        <Route path="/">
          <Login />
        </Route>
      </BrowserRouter>  
    )
  }
}

export default App;

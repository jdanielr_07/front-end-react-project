import React, { useState } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from "axios";
import "./styles/Login.css";


function Login() {

  const [username, setUsername] = useState('');
  const [zipcode, setZipCode] = useState('');
  let user;
  let sessionUser;
  const baseURL = "http://localhost:8080/users/login/" + username + "/" + zipcode;

  function handleSubmit(e) {
    e.preventDefault();

    axios.post(baseURL).then(response => {
      user = response.data.map(user => user);
      user.map(user => sessionUser = user);
      if(sessionUser){
        sessionStorage.setItem("UserId", sessionUser.id);
        window.location.reload();
      }else{
        alert("Username or Password Incorrect, Try Again!!");
      }
    })
  }

  return (
    <section class="vh-120 gradient-custom">
    <div class="container py-5 h-100">
      <div class="row d-flex justify-content-center align-items-center h-100">
        <div class="col-12 col-md-8 col-lg-6 col-lg-5">
          <div class="card bg-dark text-white">
            <div class="card-body p-5 text-center">
              <div class="mb-md-5 mt-md-4 pb-5">
                <h2 class="fw-bold mb-2 text-uppercase">Login</h2>
                <p class="text-white-50 mb-5">Please enter your Username and Password!</p>
                <div class="form-outline form-white mb-4">
                  <input type="username" id="typeEmailX" class="form-control form-control-lg" onChange={event => setUsername(event.target.value)} />
                  <label class="form-label" for="typeUsername" >Username</label>
                </div>
                <div class="form-outline form-white mb-4">
                  <input type="password" id="zipcode" class="form-control form-control-lg" onChange={event => setZipCode(event.target.value)} />
                  <label class="form-label" for="zipcode">Password</label>
                </div>
                <button onClick={(e) => handleSubmit(e)} class="btn btn-primary btn-log">Login</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  );
}

export default Login;


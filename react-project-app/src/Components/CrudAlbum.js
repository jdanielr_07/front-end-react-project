import React from "react";
import "./styles/CrudAlbum.css";

function CrudAlbum(){
    const id = 1
    const userID = sessionStorage.getItem("UserId");
    return (
        <div className='card albums-preview crud-album'>
            <div className='card-body'>
            <form class="album-form" action="http://localhost:8080/albums/insert" method="POST">
                <input type="hidden" class="form-control" id="userId" value={`${userID}`}></input>
                <input type="hidden" class="form-control" id="id" value={id}></input>
                <div class="form-group">
                    <label for="title">Name</label>
                    <input type="text" class="form-control" id="title" placeholder="Enter name"></input>
                </div>
                <br></br>
                <div class="form-group">
                    <label for="description">Description</label>
                    <input type="text" class="form-control" id="description" placeholder="Description"></input>
                </div>
                <br></br>
                <button type="submit" class="btn btn-primary">Add new album</button>
            </form>      
            </div>
        </div>
    )
}

export default CrudAlbum;
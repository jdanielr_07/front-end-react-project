import React, {useEffect, useState} from "react";
import { NavLink, useLocation } from "react-router-dom";
import User from "./User";
import "./styles/Navbar.css";
import axios from "axios";

function Navbar() { 
  const userID = sessionStorage.getItem("UserId");
  const baseURL = "http://localhost:8080/users/"+ userID;
  const [user, setUser] = useState(null);
  const [loadingUser, setLoadingUser] = useState(true);
  const [title, setTitle] = useState('');

  useEffect(() => {
      axios.get(baseURL).then(response => {
          setUser(response.data);
          setLoadingUser(false)
      })
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const usePathname = () => {
    const location = useLocation();
    return location.pathname;
  }
  let searchbytitle = true;
  let placeholderSearch = true;
  let buttonName = true;
  let path = usePathname();
  if(path !== "/"){    
    if (path === "/crudalbum"){
      //document.getElementById('button-add').style.display='none';
    }else if (path === "/crudphoto"){
      //document.getElementById('button-add').style.display='none';
      buttonName = false;
      searchbytitle = false;
      placeholderSearch = false;
    }else{
      buttonName = false;
      searchbytitle = false;
      placeholderSearch = false;
      //document.getElementById('button-add').style.display='block';
    }
  }else{
    //document.getElementById('button-add').style.display='block';
  }

  function handleSubmit(e) {
    e.preventDefault();
    window.location = "/";
    sessionStorage.clear();
    window.location.reload();
  }

  return (
    <div className="navbar navbar navbar-expand-lg navbar-dark bg-dark">
      <div className="container">
        <div className="justify-content-start">
          <div className="navbar-nav top-left">
            <NavLink to="/" exact className="nav-link" activeClassName="active">Home</NavLink>        
            {!loadingUser && user.map(user => {
                return(
                    <User
                        id={user.id}
                        name={user.name} />
                ) 
            })}
            <input class="input-search form-control mr-sm-2" type="search" placeholder={`Search ${placeholderSearch ? "Album" : "Photo"}`} activeClassName="active" onChange={event => setTitle(event.target.value)}></input>
            <NavLink to={`${searchbytitle ? "/albums" : "/photo"}?title=${title}`} className="nav-link" activeClassName="active">Search</NavLink>
          </div>
        </div>
        <div className="justify-content-end">
          <div className="navbar-nav">
            <NavLink to={`${searchbytitle ? "/crudalbum" : "/crudphoto"}`} exact className="nav-link" activeClassName="active">
                  <button className='btn btn-success' id="button-add">{`Add new ${buttonName ? "Album" : "Photo"}`} </button>
            </NavLink>
          </div>
        </div>
        <div className="justify-content-end">
          <div className="navbar-nav">
            <NavLink to="/settings" exact className="nav-link" activeClassName="active">
                  <button className='btn btn-primary'>Settings User</button>
            </NavLink>
          </div>
        </div>
        <div className="justify-content-end" id="myList">
            <div className="navbar-nav">
              <NavLink to="/" exact className="nav-link" activeClassName="active">
                <button onClick={(e) => handleSubmit(e)} className='btn btn-secondary'>Logout</button>
              </NavLink>
            </div>
        </div>
      </div>
    </div>
  );
}

export default Navbar;

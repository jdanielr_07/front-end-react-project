import React, {useEffect, useState} from "react";
import { useLocation } from "react-router-dom";
import AlbumCard from "./AlbumCard";
import queryString from 'query-string';
import axios from "axios";
import 'bootstrap/dist/css/bootstrap.min.css';

function Albums() {
  const { search } = useLocation();
  const { title } = queryString.parse(search);
  
  const baseURL = "http://localhost:8080/albums/title/" + title + "/" + sessionStorage.getItem('UserId');
  const [album, setAlbum] = useState(null);
  const [loadingAlbum, setLoadingAlbum] = useState(true);

  useEffect(() => {
        axios.get(baseURL).then(response => {
            setAlbum(response.data);
            setLoadingAlbum(false);
        })
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <div className="container mt-4">
        <br></br>
        <br></br>
        <div className="row row-cols-md-5">
            {!loadingAlbum && album.map(album => {
                return(
                    <AlbumCard
                        id={album.id}
                        title={album.title} />
                ) 
            })}
        </div>
    </div>
    )
}

export default Albums;
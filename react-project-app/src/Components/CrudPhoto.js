import React from "react";
import "./styles/CrudPhoto.css";

function CrudPhoto(props){
    const albumId = 1
    const id = 0
    return (
        <div className='card albums-preview crud-album'>
            <div className='card-body'>
            <form class="album-form" action="http://localhost:8080/photos/insert" method="POST">
                <input type="hidden" class="form-control" id="albumId" value={`${albumId}`}></input>
                <input type="hidden" class="form-control" id="id" value={id}></input>  
                <div class="form-group">
                    <label for="name">Title</label>
                    <input type="text" class="form-control" id="title" aria-describedby="emailHelp" placeholder="Enter name"></input>
                </div>
                <br></br>
                <div class="form-group">
                    <label for="url">URL</label>
                    <input type="text" class="form-control" id="url" placeholder="Description"></input>
                </div>
                <br></br>
                <div class="form-group">
                    <label for="thumbnailUrl">ThumbnailURL</label>
                    <input type="text" class="form-control" id="thumbnailUrl" placeholder="URL"></input>
                </div>
                <br></br>
                <button type="submit" class="btn btn-primary">Add new photo</button>
            </form>      
            </div>
        </div>
    )
}

export default CrudPhoto;